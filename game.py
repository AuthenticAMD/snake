import pygame
import random
import time

pygame.init()

width = 640
height = 640

window = pygame.display.set_mode((width, height))
clock = pygame.time.Clock()

size = 64
map_size = 20
snake = [{"x": 5, "y": 4}]
new_part = True
direction = "north"
food = {"x": 6, "y": 8}
running = True
while running:
    old_direction = direction
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            running = False
        elif i.type == pygame.KEYDOWN:
            if i.key == pygame.K_w:
                direction = "north"
            elif i.key == pygame.K_a:
                direction = "east"
            elif i.key == pygame.K_s:
                direction = "south"
            elif i.key == pygame.K_d:
                direction = "west"
    if direction == "north" and old_direction == "south":
        direction = old_direction
    elif direction == "east" and old_direction == "west":
        direction = old_direction
    elif direction == "south" and old_direction == "north":
        direction = old_direction
    elif direction == "west" and old_direction == "east":
        direction = old_direction

    window.fill((0, 0, 0))
    # For each element of the list called snake, going from highest to lowest,
    # if we are not at element zero, change the current element's x and y values
    # to be the ones of the one below.

    # If we are at the zeroth element, change the x and y values according to direction.

    # In practice, we can just insert one element at the beginning and pop the last one.
    snake.insert(0, dict(snake[0]))
    if direction == "north":
        snake[0]["y"] -= 1
    elif direction == "east":
        snake[0]["x"] -= 1
    elif direction == "south":
        snake[0]["y"] += 1
    elif direction == "west":
        snake[0]["x"] += 1
    if snake[0]["x"] == food["x"] and snake[0]["y"] == food["y"]:
        while True:
            food["x"] = random.randint(0, 9)
            food["y"] = random.randint(0, 9)
            if food not in snake:
                break
    else:
        snake.pop()

    if snake[0]["x"] < 0 or snake[0]["x"] > 9 or snake[0]["y"] < 0 or snake[0]["y"] > 9:
        break

    snake_copy = list(snake)
    snake_copy.pop(0)
    if snake[0] in snake_copy:
        break

    for snake_part in range(len(snake)):
        if snake_part == 0:
            pygame.draw.rect(window, (245, 245, 0), (snake[snake_part]["x"] * size, snake[snake_part]["y"] * size, size, size))
        else:
            pygame.draw.rect(window, (255, 255, 255), (snake[snake_part]["x"] * size, snake[snake_part]["y"] * size, size, size))

    pygame.draw.rect(window, (255, 100, 100), (food["x"] * size, food["y"] * size, size, size))
    pygame.display.update()
    clock.tick(3)

pygame.quit()

